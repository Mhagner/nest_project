import { Controller, Get, Param, Post, Body } from '@nestjs/common';
/* import { CatDTO } from './dto/criar-cat.dto'; */
import { CatsService } from './cats.service'
import { Cat } from './interfaces/cats.interfaces'


@Controller('cats')
export class CatsController {
    constructor(private catsService: CatsService){}

    @Post()
     async create(@Body() cat: Cat) {
        this.catsService.create(cat);
    }                       

    @Get()
    async findAll(): Promise<Cat[]> {
        return this.catsService.findAll()
    }
}
