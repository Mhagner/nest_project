import { Test, TestingModule } from '@nestjs/testing';
import { from } from 'rxjs';
import { CatsController } from './cats.controller';
import { CatsService } from './cats.service'

describe('CatsController', () => {
  let catsController: CatsController;

  beforeEach(async () => {
    const cat: TestingModule = await Test.createTestingModule({
      controllers: [CatsController],
      providers: [CatsService],
    }).compile();

    catsController = cat.get<CatsController>(CatsController);
  });

  it('should return all cats', () => {
    expect(catsController.findAll()).toBe(Promise);
  });
});
